package models;

import models.enums.Genero;
import org.junit.Assert;
import org.junit.Test;

public class AnimalTest {

    @Test
    public void instanciaAnimalTest(){
        Animal animal = new Animal();
        Assert.assertTrue(Genero.FEMENINO.equals(animal.getGenero()));
        Assert.assertFalse(Genero.MASCULINO.equals(animal.getGenero()));
    }
}
