package models.subtypes;

import models.Animal;

/**
 * Modelo que representa un {@code models.Animal}, que no consume carne.
 * @author Alberto Trevizo Saenz
 * @see models.Animal
 * @since 1.0
 * @version 1.0
 */
public class Herbivorous extends Animal{
}
