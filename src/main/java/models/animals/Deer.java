package models.animals;

import models.subtypes.Herbivorous;

/**
 * Modelo que representa un venado dentro de la aplicacion.
 * @author Alberto Trevizo Saenz
 * @see models.Animal
 * @since 1.0
 * @version 1.0
 */
public class Deer extends Herbivorous{
}
